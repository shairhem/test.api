﻿namespace NPV.Api.Test.Stubs
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using NPV.Infrastructure.Messages;
    using NPV.Infrastructure.Models;
    using NPV.Services.Npv;

    internal class StubNpvService : INpvService
    {
        private Dictionary<string, ICollection<NpvResult>> values;

        public StubNpvService()
        {
            this.values = new Dictionary<string, ICollection<NpvResult>>();
            this.values.Add("1", new List<NpvResult>());
        }

        public Task<ICollection<NpvResult>> ComputeNpvAsync(ComputeNpvRequest request)
        {
            var key = request.InitialInvestment.ToString();
            return Task.FromResult(this.values[key]);
        }
    }
}

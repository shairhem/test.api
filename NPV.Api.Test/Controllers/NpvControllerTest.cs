﻿namespace NPV.Api.Test.Controllers
{
    using System.Threading.Tasks;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NPV.Api.Controllers;
    using NPV.Api.Test.Stubs;
    using NPV.Infrastructure.Messages;
    using NPV.Services.Npv;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using NPV.Infrastructure.Models;
    using System;

    [TestClass]
    public class NpvControllerTest
    {
        private INpvService npvService;
        private NpvController target;
        private ComputeNpvRequest validRequest;
        private ComputeNpvRequest invalidRequest;
        private ComputeNpvRequest errorRequest;

        [TestInitialize]
        public void Initialize()
        {
            this.npvService = new StubNpvService();
            this.target = new NpvController(this.npvService);

            this.InitializeRequests();
        }

        private void InitializeRequests()
        {
            // initialize requests for testing
            var cashFlows = new List<CashFlow>()
            {
                new CashFlow()
                {
                    TransactionId = Guid.Empty,
                    Value = 1.0
                }
            };
            this.validRequest = new ComputeNpvRequest()
            {
                InitialInvestment = 1.0,
                InterestIncrement = 1.0,
                LowerBound = 1.0,
                UpperBound = 1.0,
                CashFlows = cashFlows
            };

            this.errorRequest = this.validRequest;
            this.errorRequest = new ComputeNpvRequest()
            {
                InitialInvestment = 2.0,
                InterestIncrement = 1.0,
                LowerBound = 1.0,
                UpperBound = 1.0,
                CashFlows = cashFlows
            };

            this.invalidRequest = new ComputeNpvRequest()
            {
                InitialInvestment = 1
            };
        }

        [TestCleanup]
        public void Cleanup()
        {
            this.npvService = null;
            this.validRequest = null;
            this.invalidRequest = null;
            this.errorRequest = null;
        }

        [TestMethod]
        public async Task Post_ModelIsValid_ReturnsNpvResults()
        {
            var input = this.validRequest;
            var result = await this.target.Post(input) as OkObjectResult;
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task Post_ModelIsNotValid_ReturnsBadRequest()
        {
            var input = this.invalidRequest;
            var result = await this.target.Post(input) as BadRequestResult;
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task Post_ErrorOccured_ReturnsInternalServerError()
        {
            var input = this.errorRequest;
            var result = await this.target.Post(input) as StatusCodeResult;
            Assert.AreEqual(result.StatusCode, 500);
        }
    }
}

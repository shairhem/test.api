﻿namespace NPV.Services.CashFlow
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using NPV.Data.DataGateways.CashFlow;
    using NPV.Infrastructure.Models;

    public class CashFlowService : ICashFlowService
    {
        private ICashFlowDataGateway gateway;


        public CashFlowService(ICashFlowDataGateway gateway)
        {
            this.gateway = gateway;
        }

        public async Task<bool> CreateCashFlowsAsync(ICollection<CashFlow> cashFlows, Guid transactionId)
        {
            var result = false;
            this.SetTransactionId(ref cashFlows, transactionId);
            foreach(CashFlow cashFlow in cashFlows)
            {
                result = await this.gateway.CreateCashFlowAsync(cashFlow);
            }

            return result;
        }

        private void SetTransactionId(ref ICollection<CashFlow> cashFlows, Guid transactionId)
        {
            cashFlows = cashFlows.Select(cashFlow =>
            {
                cashFlow.TransactionId = transactionId;
                return cashFlow;
            }).ToList();
        }
    }
}

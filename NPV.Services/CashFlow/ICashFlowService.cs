﻿namespace NPV.Services.CashFlow
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Infrastructure.Models;

    public interface ICashFlowService
    {
        Task<bool> CreateCashFlowsAsync(ICollection<CashFlow> cashFlows, Guid transactionId);
    }
}

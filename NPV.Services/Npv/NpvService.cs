﻿namespace NPV.Services.Npv
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using NPV.Data.DataGateways.Transaction;
    using NPV.Infrastructure.Extensions;
    using NPV.Infrastructure.Messages;
    using NPV.Infrastructure.Models;
    using NPV.Services.CashFlow;
    using NPV.Services.NpvResult;

    public class NpvService : INpvService
    {
        private ITransactionDataGateway transactionGateway;
        private ICashFlowService cashFlowService;
        private INpvResultService npvResultService;

        public NpvService(ITransactionDataGateway transactionGateway, ICashFlowService cashFlowService, INpvResultService npvResultService)
        {
            this.transactionGateway = transactionGateway;
            this.cashFlowService = cashFlowService;
            this.npvResultService = npvResultService;
        }

        public async Task<ICollection<NpvResult>> ComputeNpvAsync(ComputeNpvRequest request)
        {
            var result = new List<NpvResult>();
            var cashFlows = request.CashFlows;
            var currentDiscountRate = request.LowerBound;
            double currentNpv;
            do
            {
                currentNpv = this.computeNpv(request.CashFlows, request.InitialInvestment, currentDiscountRate.ToDecimal());
                result.Add(new NpvResult() {
                    Rate = currentDiscountRate,
                    Npv = currentNpv
                });
                currentDiscountRate += request.InterestIncrement;
            }
            while (currentDiscountRate <= request.UpperBound);

            await this.SaveDataAsync(request, result);

            return result;
        }

        private double computeNpv(List<CashFlow> cashFlows, double initialInvestment, double interest)
        {
            double result = 0.0;
            for (var itr = 0; itr < cashFlows.Count; itr++)
            {
                var cashFlow = (cashFlows[itr].Value);
                var dividend = Math.Pow(interest + 1, itr+1);
                var value = cashFlow / dividend;
                result += value;
            }
            result -= initialInvestment;
            result = Math.Round(result, 3);
            return result;
        }

        private async Task SaveDataAsync(ComputeNpvRequest request, ICollection<NpvResult> npvResults)
        {
            var transaction = request.GetTransaction();
            bool transactionResult = await this.transactionGateway.CreateTransactionAsync(transaction);
            if (transactionResult)
            {
                await this.cashFlowService.CreateCashFlowsAsync(request.CashFlows, transaction.Id);
                await this.npvResultService.CreateNpvResultsAsync(npvResults, transaction.Id);
            }
            
        }
    }
}

﻿namespace NPV.Services.Npv
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using NPV.Infrastructure.Messages;
    using NPV.Infrastructure.Models;

    public interface INpvService
    {
        Task<ICollection<NpvResult>> ComputeNpvAsync(ComputeNpvRequest request);
    }
}

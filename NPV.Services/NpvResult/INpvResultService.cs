﻿namespace NPV.Services.NpvResult
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using NPV.Infrastructure.Models;

    public interface INpvResultService
    {
        Task<bool> CreateNpvResultsAsync(ICollection<NpvResult> npvResults, Guid transactionId);

        Task<ICollection<NpvResult>> GetNpvResultsByTransactionIdAsync(Guid transactionId);
    }
}

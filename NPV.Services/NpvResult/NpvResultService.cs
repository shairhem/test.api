﻿namespace NPV.Services.NpvResult
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using NPV.Data.DataGateways.Npv;
    using NPV.Infrastructure.Models;

    public class NpvResultService : INpvResultService
    {
        private INpvResultDataGateway gateway;

        public NpvResultService(INpvResultDataGateway gateway)
        {
            this.gateway = gateway;
        }

        public async Task<bool> CreateNpvResultsAsync(ICollection<NpvResult> npvResults, Guid transactionId)
        {
            var result = false;
            this.SetTransactionId(ref npvResults, transactionId);
            foreach (var npvResult in npvResults)
            {
                result = await this.gateway.CreateNpvResultAsync(npvResult);
            }

            return result;
        }

        public async Task<ICollection<NpvResult>> GetNpvResultsByTransactionIdAsync(Guid transactionId)
        {
            var result = await this.gateway.GetNpvResultsByTransactionIdAsync(transactionId);

            return result;
        }

        private void SetTransactionId(ref ICollection<NpvResult> npvResults, Guid transactionId)
        {
            npvResults = npvResults.Select(cashFlow =>
            {
                cashFlow.TransactionId = transactionId;
                return cashFlow;
            }).ToList();
        }
    }
}

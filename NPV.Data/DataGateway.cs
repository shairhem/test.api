﻿namespace NPV.Data
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Options;
    using NPV.Infrastructure.Config;

    public class DataGateway : IDataGateway
    {
        private string connectionString;

        public DataGateway(IOptions<Config> config)
        {
            this.connectionString = config.Value.ConnectionString;
        }

        public async Task<bool> ExecuteNonQueryAsync(SqlCommand command)
        {
            bool result = false;
            using (var connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction(IsolationLevel.Serializable))
                {
                    try
                    {
                        command.Connection = connection;
                        command.Transaction = transaction;

                        await command.ExecuteNonQueryAsync();
                        result = true;
                        transaction.Commit();
                    }
                    catch (Exception exception)
                    {
                        transaction.Rollback();
                        throw exception;
                    }
                }
            }

            return result;
        }

        public Task<DataTable> ExecuteQueryAsync(SqlCommand command)
        {
            DataTable result = new DataTable();
            using (var connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction(IsolationLevel.Serializable))
                {
                    try
                    {
                        command.Connection = connection;
                        command.Transaction = transaction;

                        using (var dataAdapter = new SqlDataAdapter(command))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            dataAdapter.SelectCommand = command;
                            dataAdapter.Fill(result);
                        }
                    }
                    catch (Exception exception)
                    {
                        transaction.Rollback();
                        throw exception;
                    }
                }
            }

            return Task.FromResult(result);
        }

        public async Task<DataTable> ExecuteUpsertQueryAsync(SqlCommand command)
        {
            DataTable result = new DataTable();
            using (var connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction(IsolationLevel.Serializable))
                {
                    try
                    {
                        command.Connection = connection;
                        command.Transaction = transaction;

                        var reader = await command.ExecuteReaderAsync();
                        if (reader.HasRows)
                        {
                            result.Load(reader);
                        }

                        reader.Close();
                        transaction.Commit();
                    }
                    catch (Exception exception)
                    {
                        transaction.Rollback();
                        throw exception;
                    }
                }
            }

            return result;
        }
    }
}

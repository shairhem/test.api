﻿namespace NPV.Data
{
    using System.Data;
    using System.Data.SqlClient;
    using System.Threading.Tasks;

    public interface IDataGateway
    {
        Task<DataTable> ExecuteQueryAsync(SqlCommand command);

        Task<bool> ExecuteNonQueryAsync(SqlCommand command);

        Task<DataTable> ExecuteUpsertQueryAsync(SqlCommand command);
    }
}

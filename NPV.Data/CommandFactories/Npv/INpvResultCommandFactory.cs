﻿namespace NPV.Data.CommandFactories.Npv
{
    using System;
    using System.Data.SqlClient;
    using NPV.Infrastructure.Models;

    public interface INpvResultCommandFactory
    {
        SqlCommand CreateNpvResult(NpvResult npvResult);

        SqlCommand GetNpvResultsByTransactionId(Guid transactionId);
    }
}

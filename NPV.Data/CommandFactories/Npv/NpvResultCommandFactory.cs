﻿namespace NPV.Data.CommandFactories.Npv
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using NPV.Infrastructure.Models;

    public class NpvResultCommandFactory : INpvResultCommandFactory
    {
        public SqlCommand CreateNpvResult(NpvResult npvResult)
        {
            var command = new SqlCommand("spCreateNpvResult");
            command.CommandType = CommandType.StoredProcedure;
            command.CommandTimeout = 10;
            command.Parameters.AddWithValue("@TransactionId", npvResult.TransactionId);
            command.Parameters.AddWithValue("@Rate", npvResult.Rate);
            command.Parameters.AddWithValue("@Npv", npvResult.Npv);

            return command;
        }

        public SqlCommand GetNpvResultsByTransactionId(Guid transactionId)
        {
            var command = new SqlCommand("spGetNpvResultByTransactionId");
            command.CommandType = CommandType.StoredProcedure;
            command.CommandTimeout = 10;
            command.Parameters.AddWithValue("@TransactionId", transactionId);

            return command;
        }
    }
}

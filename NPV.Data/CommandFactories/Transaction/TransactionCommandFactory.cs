﻿namespace NPV.Data.CommandFactories.Transaction
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using NPV.Infrastructure.Models;

    public class TransactionCommandFactory : ITransactionCommandFactory
    {
        public SqlCommand CreateTransaction(Transaction transaction)
        {
            var command = new SqlCommand("spCreateTransaction");
            command.CommandType = CommandType.StoredProcedure;
            command.CommandTimeout = 10;
            command.Parameters.AddWithValue("@Id", transaction.Id);
            command.Parameters.AddWithValue("@InitialInvestment", transaction.InitialInvestment);
            command.Parameters.AddWithValue("@LowerBound", transaction.LowerBound);
            command.Parameters.AddWithValue("@UpperBound", transaction.UpperBound);
            command.Parameters.AddWithValue("@DiscountRateIncrement", transaction.DiscountRateIncrement);

            return command;
        }

        public SqlCommand GetTransactions()
        {
            var command = new SqlCommand("spGetTransactions");
            command.CommandType = CommandType.StoredProcedure;
            command.CommandTimeout = 10;

            return command;
        }
    }
}

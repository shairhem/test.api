﻿namespace NPV.Data.CommandFactories.Transaction
{
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using Infrastructure.Models;

    public interface ITransactionCommandFactory
    {
        SqlCommand CreateTransaction(Transaction transaction);

        SqlCommand GetTransactions();
    }
}

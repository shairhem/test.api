﻿namespace NPV.Data.CommandFactories.CashFlow
{
    using System.Data.SqlClient;
    using Infrastructure.Models;

    public interface ICashFlowCommandFactory
    {
        SqlCommand CreateCashFlow(CashFlow cashFlow);

        SqlCommand GetCashFlowsByTransactionId(int transactionId);
    }
}

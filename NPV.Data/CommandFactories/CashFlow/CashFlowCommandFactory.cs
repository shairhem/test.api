﻿namespace NPV.Data.CommandFactories.CashFlow
{
    using System.Data;
    using System.Data.SqlClient;
    using Infrastructure.Models;

    public class CashFlowCommandFactory : ICashFlowCommandFactory
    {
        public SqlCommand CreateCashFlow(CashFlow cashFlow)
        {
            var command = new SqlCommand("spCreateCashFlow");
            command.CommandType = CommandType.StoredProcedure;
            command.CommandTimeout = 10;
            command.Parameters.AddWithValue("@TransactionId", cashFlow.TransactionId);
            command.Parameters.AddWithValue("@Value", cashFlow.Value);

            return command;
        }

        public SqlCommand GetCashFlowsByTransactionId(int transactionId)
        {
            var command = new SqlCommand("spGetCashFlowsByTransactionId");
            command.CommandType = CommandType.StoredProcedure;
            command.CommandTimeout = 10;

            return command;
        }
    }
}

﻿namespace NPV.Data.DataGateways.Transaction
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using NPV.Data.CommandFactories.Transaction;
    using NPV.Infrastructure.Models;

    public class TransactionDataGateway : ITransactionDataGateway
    {
        private IDataGateway gateway;
        private ITransactionCommandFactory commandFactory;

        public TransactionDataGateway(IDataGateway gateway, ITransactionCommandFactory commandFactory)
        {
            this.gateway = gateway;
            this.commandFactory = commandFactory;
        }

        public async Task<bool> CreateTransactionAsync(Transaction transaction)
        {
            var command = this.commandFactory.CreateTransaction(transaction);
            var result = await this.gateway.ExecuteNonQueryAsync(command);

            return result;
        }

        public async Task<ICollection<Transaction>> GetTransactionsAsync()
        {
            ICollection<Transaction> transactions = new List<Transaction>();
            var command = this.commandFactory.GetTransactions();
            var result = await this.gateway.ExecuteQueryAsync(command);
            if (result != null)
            {
                foreach (DataRow dataRow in result.Rows)
                {
                    transactions.Add(BuilTransactionFromSqlRows(dataRow));
                }
            }

            return transactions;
        }

        private Transaction BuilTransactionFromSqlRows(DataRow dataRow)
        {
            var result = new Transaction();
            result.Id = Guid.Parse(dataRow[0].ToString());
            result.InitialInvestment = Convert.ToDouble(dataRow[1].ToString());
            result.LowerBound = Convert.ToDouble(dataRow[2].ToString());
            result.UpperBound = Convert.ToDouble(dataRow[3].ToString());
            result.DiscountRateIncrement = Convert.ToDouble(dataRow[4].ToString());
            result.DateCreated = Convert.ToDateTime(dataRow[5].ToString());

            return result;
        }
    }
}

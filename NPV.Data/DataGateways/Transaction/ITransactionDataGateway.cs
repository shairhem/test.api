﻿namespace NPV.Data.DataGateways.Transaction
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Infrastructure.Models;

    public interface ITransactionDataGateway
    {
        Task<bool> CreateTransactionAsync(Transaction transaction);

        Task<ICollection<Transaction>> GetTransactionsAsync();
    }
}

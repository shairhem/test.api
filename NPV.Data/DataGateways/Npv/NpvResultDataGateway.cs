﻿namespace NPV.Data.DataGateways.Npv
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using NPV.Data.CommandFactories.Npv;
    using NPV.Infrastructure.Models;

    public class NpvResultDataGateway : INpvResultDataGateway
    {
        private IDataGateway dataGateway;
        private INpvResultCommandFactory commandFactory;

        public NpvResultDataGateway(IDataGateway dataGateway, INpvResultCommandFactory commandFactory)
        {
            this.dataGateway = dataGateway;
            this.commandFactory = commandFactory;
        }

        public async Task<bool> CreateNpvResultAsync(NpvResult npvResult)
        {
            var command = this.commandFactory.CreateNpvResult(npvResult);
            var result = await this.dataGateway.ExecuteNonQueryAsync(command);

            return result;
        }

        public async Task<ICollection<NpvResult>> GetNpvResultsByTransactionIdAsync(Guid transactionId)
        {
            ICollection<NpvResult> npvResults = new List<NpvResult>();
            var command = this.commandFactory.GetNpvResultsByTransactionId(transactionId);
            var result = await this.dataGateway.ExecuteQueryAsync(command);
            if (result != null)
            {
                foreach (DataRow dataRow in result.Rows)
                {
                    npvResults.Add(BuildNpvResultFromSqlRows(dataRow));
                }
            }

            return npvResults;
        }

        private NpvResult BuildNpvResultFromSqlRows(DataRow dataRow)
        {
            var result = new NpvResult();
            result.Id = Convert.ToInt32(dataRow[0].ToString());
            result.TransactionId = Guid.Parse(dataRow[1].ToString());
            result.Rate = Convert.ToDouble(dataRow[2].ToString());
            result.Npv = Convert.ToDouble(dataRow[3].ToString());

            return result;
        }
    }
}

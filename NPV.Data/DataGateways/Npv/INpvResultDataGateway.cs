﻿namespace NPV.Data.DataGateways.Npv
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using NPV.Infrastructure.Models;

    public interface INpvResultDataGateway
    {
        Task<bool> CreateNpvResultAsync(NpvResult npvResult);

        Task<ICollection<NpvResult>> GetNpvResultsByTransactionIdAsync(Guid transactionId);
    }
}

﻿namespace NPV.Data.DataGateways.CashFlow
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using NPV.Data.CommandFactories.CashFlow;
    using NPV.Infrastructure.Models;

    public class CashFlowDataGateway : ICashFlowDataGateway
    {
        private IDataGateway gateway;
        private ICashFlowCommandFactory commandFactory;

        public CashFlowDataGateway(IDataGateway gateway, ICashFlowCommandFactory commandFactory)
        {
            this.gateway = gateway;
            this.commandFactory = commandFactory;
        }

        public async Task<bool> CreateCashFlowAsync(CashFlow cashFlow)
        {
            var command = this.commandFactory.CreateCashFlow(cashFlow);
            var result = await this.gateway.ExecuteNonQueryAsync(command);

            return result;
        }

        public async Task<ICollection<CashFlow>> GetCashFlowByTransactionIdAsync(int transactionId)
        {
            ICollection<CashFlow> cashFlows = new List<CashFlow>();
            var command = this.commandFactory.GetCashFlowsByTransactionId(transactionId);
            var result = await this.gateway.ExecuteQueryAsync(command);
            if (result != null)
            {
                foreach (DataRow dataRow in result.Rows)
                {
                    cashFlows.Add(BuildCashFlowFromSqlRows(dataRow));
                }
            }

            return cashFlows;
        }

        private CashFlow BuildCashFlowFromSqlRows(DataRow dataRow)
        {
            var result = new CashFlow();
            result.Id = Convert.ToInt32(dataRow[0].ToString());
            result.TransactionId = Guid.Parse(dataRow[1].ToString());
            result.Value = Convert.ToDouble(dataRow[2].ToString());
            result.DateCreated = Convert.ToDateTime(dataRow[3].ToString());

            return result;
        }
    }
}

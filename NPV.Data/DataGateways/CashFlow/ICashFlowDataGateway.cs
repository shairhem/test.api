﻿namespace NPV.Data.DataGateways.CashFlow
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Infrastructure.Models;

    public interface ICashFlowDataGateway
    {
        Task<bool> CreateCashFlowAsync(CashFlow cashFlow);

        Task<ICollection<CashFlow>> GetCashFlowByTransactionIdAsync(int transactionId);
    }
}

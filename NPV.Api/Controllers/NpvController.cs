﻿namespace NPV.Api.Controllers
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using NPV.Infrastructure.Messages;
    using NPV.Services.Npv;

    [Route("api/npv")]
    [ApiController]
    public class NpvController : ControllerBase
    {
        private INpvService npvService;

        public NpvController(INpvService npvService)
        {
            this.npvService = npvService;
        }

        [HttpPost]
        public async Task<ActionResult> Post(ComputeNpvRequest request)
        {
            try
            {
                var result = await this.npvService.ComputeNpvAsync(request);

                return this.Ok(result);
            }
            catch (Exception ex)
            {
                return this.StatusCode(500);
            }
        }
    }
}
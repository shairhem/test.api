﻿namespace NPV.Api
{
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using NPV.Data;
    using NPV.Data.CommandFactories.CashFlow;
    using NPV.Data.CommandFactories.Npv;
    using NPV.Data.CommandFactories.Transaction;
    using NPV.Data.DataGateways.CashFlow;
    using NPV.Data.DataGateways.Npv;
    using NPV.Data.DataGateways.Transaction;
    using NPV.Infrastructure.Config;
    using NPV.Services.CashFlow;
    using NPV.Services.Npv;
    using NPV.Services.NpvResult;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddDataAnnotationsLocalization();
            services.Configure<Config>(Configuration.GetSection("Configuration"));

            // Command Factories
            services.AddScoped<ICashFlowCommandFactory, CashFlowCommandFactory>();
            services.AddScoped<ITransactionCommandFactory, TransactionCommandFactory>();
            services.AddScoped<INpvResultCommandFactory, NpvResultCommandFactory>();

            // Data Gateways
            services.AddScoped<IDataGateway, DataGateway>();
            services.AddScoped<ICashFlowDataGateway, CashFlowDataGateway>();
            services.AddScoped<ITransactionDataGateway, TransactionDataGateway>();
            services.AddScoped<INpvResultDataGateway, NpvResultDataGateway>();

            // Services
            services.AddScoped<INpvService, NpvService>();
            services.AddScoped<ICashFlowService, CashFlowService>();
            services.AddScoped<INpvResultService, NpvResultService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Allowing all request for now
            app.UseCors("AllowAll");
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}

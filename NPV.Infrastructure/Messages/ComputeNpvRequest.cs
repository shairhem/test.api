﻿namespace NPV.Infrastructure.Messages
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Newtonsoft.Json;
    using NPV.Infrastructure.Attributes;
    using NPV.Infrastructure.Models;

    public class ComputeNpvRequest
    {
        [JsonProperty("initialInvestment")]
        [Required]
        [PositiveNumber(ErrorMessage = "Value should be greater than 0")]
        public double InitialInvestment { get; set; }

        [JsonProperty("lowerBound")]
        [Required]
        [PositiveNumber(ErrorMessage = "Value should be greater than 0")]
        public double LowerBound { get; set; }

        [JsonProperty("upperBound")]
        [Required]
        [PositiveNumber(ErrorMessage = "Value should be greater than 0")]
        public double UpperBound { get; set; }

        [JsonProperty("increment")]
        [Required]
        [PositiveNumber(ErrorMessage = "Value should be greater than 0")]
        public double InterestIncrement { get; set; }

        [JsonProperty("cashFlows")]
        [Required]
        public List<CashFlow> CashFlows { get; set; }
    }
}

﻿namespace NPV.Infrastructure.Extensions
{
    using System;
    using NPV.Infrastructure.Messages;
    using NPV.Infrastructure.Models;

    public static class ComputeNpvRequestExtensions
    {
        public static Transaction GetTransaction(this ComputeNpvRequest request)
        {
            var result = new Transaction();
            result.Id = Guid.NewGuid();
            result.InitialInvestment = request.InitialInvestment;
            result.LowerBound = request.LowerBound;
            result.UpperBound = request.UpperBound;
            result.DiscountRateIncrement = request.InterestIncrement;

            return result;
        }
    }
}

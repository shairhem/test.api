﻿namespace NPV.Infrastructure.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using NPV.Infrastructure.Models;

    public static class CashFlowExtensions
    {
        public static ICollection<CashFlow> SetTransactionId(this ICollection<CashFlow> cashFlows, Guid transactionId)
        {
            var result = cashFlows.Select(cashFlow =>
            {
                cashFlow.TransactionId = transactionId;
                return cashFlow;
            }).ToList();

            return result;
        }
    }
}

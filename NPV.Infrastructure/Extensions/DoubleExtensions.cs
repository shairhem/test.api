﻿namespace NPV.Infrastructure.Extensions
{
    public static class DoubleExtensions
    {
        public static double ToDecimal(this double value)
        {
            var result = value / 100;

            return result;
        }
    }
}

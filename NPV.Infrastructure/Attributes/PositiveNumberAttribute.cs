﻿namespace NPV.Infrastructure.Attributes
{
    using System.ComponentModel.DataAnnotations;

    public class PositiveNumberAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var input = (double)value;
            var result = input > 0;

            return result;
        }
    }
}

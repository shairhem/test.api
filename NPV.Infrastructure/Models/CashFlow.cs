﻿namespace NPV.Infrastructure.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using Newtonsoft.Json;

    public class CashFlow
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("transactionId")]
        [Required]
        public Guid TransactionId { get; set; }

        [JsonProperty("value")]
        [Required]
        public double Value { get; set; }

        [JsonProperty("dateCreated")]
        public DateTime DateCreated { get; set; }
    }
}

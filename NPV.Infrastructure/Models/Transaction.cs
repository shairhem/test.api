﻿namespace NPV.Infrastructure.Models
{
    using System;
    using Newtonsoft.Json;

    public class Transaction
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("initialInvestment")]
        public double InitialInvestment { get; set; }

        [JsonProperty("lowerBound")]
        public double LowerBound { get; set; }

        [JsonProperty("upperBound")]
        public double UpperBound { get; set; }

        [JsonProperty("increment")]
        public double DiscountRateIncrement { get; set; }

        [JsonProperty("dateCreated")]
        public DateTime DateCreated { get; set; }
    }
}

﻿namespace NPV.Infrastructure.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Newtonsoft.Json;

    public class NpvResult
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("transactionId")]
        public Guid TransactionId { get; set; }

        [JsonProperty("rate")]
        public double Rate { get; set; }

        [JsonProperty("npv")]
        public double Npv { get; set; }
    }
}

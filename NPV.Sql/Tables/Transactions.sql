﻿CREATE TABLE [dbo].[Transactions]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY DEFAULT NEWID(), 
    [InitialInvestment] DECIMAL(18, 4) NOT NULL, 
    [LowerBound] DECIMAL(18, 4) NOT NULL, 
    [UpperBound] DECIMAL(18, 4) NOT NULL, 
    [DiscountRateIncrement] DECIMAL(18, 4) NOT NULL, 
    [DateCreated] DATETIME NOT NULL DEFAULT GETDATE()
)

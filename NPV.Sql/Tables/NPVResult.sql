﻿CREATE TABLE [dbo].[NpvResult]
(
	[Id] INT IDENTITY(1,1) NOT NULL PRIMARY KEY , 
    [TransactionId] UNIQUEIDENTIFIER NOT NULL, 
    [Rate] DECIMAL(18, 4) NOT NULL, 
    [Npv] DECIMAL(18, 4) NOT NULL, 
    CONSTRAINT [FK_NPVResult_Transaction] FOREIGN KEY ([TransactionId]) REFERENCES [Transactions]([Id])
)

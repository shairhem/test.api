﻿CREATE TABLE [dbo].[CashFlow]
(
	[Id] INT IDENTITY(1,1) NOT NULL PRIMARY KEY, 
    [TransactionId] UNIQUEIDENTIFIER NOT NULL, 
    [Value] DECIMAL(18, 4) NOT NULL, 
    [DateCreated] DATETIME NOT NULL DEFAULT GETDATE(), 
    CONSTRAINT [FK_CashFlow_Transactions] FOREIGN KEY ([TransactionId]) REFERENCES [Transactions]([Id])
)

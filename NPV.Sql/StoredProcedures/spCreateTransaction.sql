﻿CREATE PROCEDURE [dbo].[spCreateTransaction]
	@Id uniqueidentifier,
	@InitialInvestment decimal(18,4),
	@LowerBound decimal(18,4),
	@UpperBound decimal(18,4),
	@DiscountRateIncrement decimal(18,4)
AS
	BEGIN
		INSERT INTO Transactions(Id, InitialInvestment, LowerBound, UpperBound, DiscountRateIncrement)
		VALUES (@Id, @InitialInvestment, @LowerBound, @UpperBound, @DiscountRateIncrement);
	END

﻿CREATE PROCEDURE [dbo].[spGetNpvResultByTransactionId]
	@TransactionId uniqueidentifier
AS
	BEGIN
		SELECT * FROM NpvResult where TransactionId = @TransactionId;
	END

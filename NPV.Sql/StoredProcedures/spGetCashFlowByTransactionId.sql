﻿CREATE PROCEDURE [dbo].[spGetCashFlowByTransactionId]
	@TransactionId int
AS
	BEGIN
		SELECT * FROM CashFlow where TransactionId = @TransactionId;
	END

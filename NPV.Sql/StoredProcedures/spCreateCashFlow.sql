﻿CREATE PROCEDURE [dbo].[spCreateCashFlow]
	@TransactionId uniqueidentifier,
	@Value decimal(18,4)
AS
	BEGIN
		INSERT INTO CashFlow(TransactionId, Value) Values (@TransactionId, @Value);
	END

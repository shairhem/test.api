﻿CREATE PROCEDURE [dbo].[spCreateNpvResult]
	@TransactionId uniqueidentifier,
	@Rate decimal(18,4),
	@Npv decimal(18,4)
AS
	BEGIN
		INSERT INTO NpvResult(TransactionId, Rate, Npv) VALUES
		(@TransactionId, @Rate, @Npv);
	END
